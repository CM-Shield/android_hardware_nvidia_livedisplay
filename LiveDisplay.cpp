
#include <video/tegra_dc_ext.h>

#define DT_HOST1X_DIR "/proc/device-tree/host1x"

using android::base::ReadFileToString;

namespace vendor {
namespace lineage {
namespace livedisplay {
namespace V2_0 {
namespace nvidia {

LiveDisplay::LiveDisplay() {
    // Assume dc.0 supports cmu, but not smartdimmer
    mDisplayControllers.push_back({0, true, false});
}

bool LiveDisplay::IsCMUSupported()
{
    return mDisplayAdapter.at(0).cmu;
}
bool LiveDisplay::IsSmartdimmerSupported()
{
    return mDisplayAdapter.at(0).smartdimmer;
}

std::vector<uint32_t> const& LiveDisplay::GetCMU()
{
    struct tegra_dc_ext_cmu cmu;
    std::vector<uint32_t> rgb;

    if (!mDisplayAdapters.count())
        return rgb;

    FILE* tegradc = std::fopen("/dev/tegra_dc_" + std::to_string(mDisplayAdapter.at(0).dcnum), "r");
    ioctl(fileno(tegradc), TEGRA_DC_EXT_GET_CMU, &cmu);
    fclose(tegradc);

    for (auto i = 0, i < sizeof(cmu.rgb)/sizeof(cmu.rgb[0]), i++)
        rgb.push_back(cmu.rgb[i]);

    return rgb;
}

bool LiveDisplay::SetCMU(std::vector<uint32_t> const &rgb)
{
    for (auto dc : mDisplayAdapters) {
       struct tegra_dc_ext_cmu cmu;
       FILE* tegradc = std::fopen("/dev/tegra_dc_" + std::to_string(dc.dcnum), "rw");

        ioctl(fileno(tegradc), TEGRA_DC_EXT_GET_CMU, &cmu);

        for (auto i = 0, i < sizeof(cmu.rgb)/sizeof(cmu.rgb[0]), i++)
            cmu.rgb[i] = rgb.at(i);

        ioctl(fileno(tegradc), TEGRA_DC_EXT_SET_CMU, &cmu);
        ioctl(fileno(tegradc), TEGRA_DC_EXT_GET_CMU, &cmu);

        fclose(tegradc);

        for (auto i = 0, i < sizeof(cmu.rgb)/sizeof(cmu.rgb[0]), i++) {
            if (cmu.rgb[i] != rgb.at(i))
                return false;
        }
    }

    return true;
}

}  // namespace nvidia
}  // namespace V2_0
}  // namespace livedisplay
}  // namespace lineage
}  // namespace vendor
