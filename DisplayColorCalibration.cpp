/*
 * Copyright (C) 2019 The LineageOS Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "DisplayColorCalibration.h"
#include "LiveDisplay.h"

namespace vendor {
namespace lineage {
namespace livedisplay {
namespace V2_0 {
namespace nvidia {

bool DisplayColorCalibration::isSupported() {
    return LiveDisplay::IsCMUSupported();
}

// Methods from ::vendor::lineage::livedisplay::V2_0::IDisplayColorCalibration follow.
Return<int32_t> DisplayColorCalibration::getMaxValue() {
    return 255;
}

Return<int32_t> DisplayColorCalibration::getMinValue() {
    return 0;
}

Return<void> DisplayColorCalibration::getCalibration(getCalibration_cb _hidl_cb) {
    std::vector<int32_t> rgb_full, rgb;

    rgb_full = LiveDisplay::GetCMU();
    rgb.push_back(rgb_full.at(0));
    rgb.push_back(rgb_full.at(4));
    rgb.push_back(rgb_full.at(8));

    _hidl_cb(rgb);
    return Void();
}

Return<bool> DisplayColorCalibration::setCalibration(const hidl_vec<int32_t>& rgb) {
    std::vector<uint32_t> csc = {rgb.at(0), 0, 0, 0, rgb.at(1), 0, 0, 0, rgb.at(2)};

    return LiveDisplay::SetCMU(csc);
}

}  // namespace nvidia
}  // namespace V2_0
}  // namespace livedisplay
}  // namespace lineage
}  // namespace vendor
